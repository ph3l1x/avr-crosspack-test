/* Name: main.c
 * Author: <insert your name here>
 * Copyright: <insert your copyright message here>
 * License: <insert your license reference here>
 */

#include <avr/io.h>
#include <util/delay.h>
#include <util/twi.h>

int main(void)
{
    DDRC = 0x1;            // make port c outputs
    PORTC = 0x0;           // switch port c on

    for(;;){
        _delay_ms(200);
        PORTC = 0x0;
        _delay_ms(50);
        PORTC = 0x1;
    }
    return 0;
}
